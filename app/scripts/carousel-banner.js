$(function() {
    auto_right_banner('.carousel-banner:first');
})

function auto_right_banner(carusel){
    setInterval(function(){
        if (!$(carusel).is('.hover'))
            right_carusel_banner(carusel);
    }, 5000)
}

function right_carusel_banner(carusel){
    var block_width = $(carusel).find('.carousel-banner__block').outerWidth();
    $(carusel).find(".carousel-banner__items").animate({left: "-"+ block_width +"px"}, 200, function(){
        $(carusel).find(".carousel-banner__items .carousel-banner__block").eq(0).clone().appendTo($(carusel).find(".carousel-banner__items"));
        $(carusel).find(".carousel-banner__items .carousel-banner__block").eq(0).remove();
        $(carusel).find(".carousel-banner__items").css({"left":"0px"});
    });
}

$(document).on('mouseenter', '.carousel-banner', function(){$(this).addClass('hover')})

$(document).on('mouseleave', '.carousel-banner', function(){$(this).removeClass('hover')})