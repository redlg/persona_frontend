$(document).on('click', ".carousel-brand__button_right",function(){
    let carusel = $(this).parents('.carousel-brand');
    right_carusel(carusel);
    return false;
});

$(document).on('click',".carousel-brand__button_left",function(){
    let carusel = $(this).parents('.carousel-brand');
    left_carusel(carusel);
    return false;
});

function left_carusel(carusel){
    let block_width = $(carusel).find('.carousel__block').outerWidth();
    $(carusel).find(".carousel-brand__items .carousel-brand__block").eq(-1).clone().prependTo($(carusel).find(".carousel-brand__items"));
    $(carusel).find(".carousel-brand__items").css({"left":"-"+block_width+"px"});
    $(carusel).find(".carousel-brand__items .carousel-brand__block").eq(-1).remove();
    $(carusel).find(".carousel-brand__items").animate({left: "0px"}, 200);

}

function right_carusel(carusel){
    let block_width = $(carusel).find('.carousel-brand__block').outerWidth();
    $(carusel).find(".carousel-brand__items").animate({left: "-"+ block_width +"px"}, 200, function(){
        $(carusel).find(".carousel-brand__items .carousel-brand__block").eq(0).clone().appendTo($(carusel).find(".carousel-brand__items"));
        $(carusel).find(".carousel-brand__items .carousel-brand__block").eq(0).remove();
        $(carusel).find(".carousel-brand__items").css({"left":"0px"});
    });
}