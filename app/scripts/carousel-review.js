$(document).ready(function() {
    let slides = $(".carousel-review__items").children(".carousel-review__block"); // Получаем массив всех слайдов
    let x = $(".carousel-review__wrapper").width(); // Получаем ширину видимой области
    let width = 0;
    let i = slides.length; // Получаем количество слайдов
    if(window.matchMedia('(max-width: 1024px)').matches) {
        width = x + 150;
        for (j=0; j < i; j++) {
            if (j==0) {
                $(".carousel-review .pagination").append("<div class='pagination__dot pagination__dot_active'></div>");
            }
            else {
                $(".carousel-review .pagination").append("<div class='pagination__dot'></div>");
            }
        }
    }
    else {
        width = x + 32;
        y = Math.round(i / 2);
        for (j=0; j < y; j++) {
            if (j==0) {
                $(".carousel-review .pagination").append("<div class='pagination__dot pagination__dot_active'></div>");
            }
            else {
                $(".carousel-review .pagination").append("<div class='pagination__dot'></div>");
            }
        }
    }
    let offset = width; // Задаем начальное смещение и ширину всех слайдов
    let cheki = i-1;

    let dots = $(".carousel-review .pagination").children(".pagination__dot");
    offset = 0; // Обнуляем смещение, так как показывается начала 1 слайд
    i = 0; // Обнуляем номер текущего слайда

    $(".carousel-review .pagination .pagination__dot").click(function(){
        $(".carousel-review .pagination .pagination__dot_active").removeClass("pagination__dot_active");
        $(this).addClass("pagination__dot_active");
        i = $(this).index();
        offset = i * width;
        $(".carousel-review__items").css("transform","translate3d(-"+offset+"px, 0px, 0px)"); // Смещаем блок со слайдами к следующему
    });


    $(".carousel-review__button_right").click(function(){
        if (offset < width * cheki) {	// Проверяем, дошли ли мы до конца
            offset += width; // Увеличиваем смещение до следующего слайда
            $(".carousel-review__items").css("transform","translate3d(-"+offset+"px, 0px, 0px)"); // Смещаем блок со слайдами к следующему
            $(".carousel-review .pagination .pagination__dot_active").removeClass("pagination__dot_active");
            $(dots[++i]).addClass("pagination__dot_active");
        }
    });


    $(".carousel-review__button_left").click(function(){
        if (offset > 0) { // Проверяем, дошли ли мы до конца
            offset -= width; // Уменьшаем смещение до предыдущегоо слайда
            $(".carousel-review__items").css("transform","translate3d(-"+offset+"px, 0px, 0px)"); // Смещаем блок со слайдами к предыдущему
            $(".carousel-review .pagination .pagination__dot_active").removeClass("pagination__dot_active");
            $(dots[--i]).addClass("pagination__dot_active");
        }
    });

});