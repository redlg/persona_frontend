$(document).on('click', ".carousel-viewed__button_right",function(){
    let carusel = $(this).parents('.carousel-viewed');
    right_carusel_viewed(carusel);
    return false;
});

$(document).on('click',".carousel-viewed__button_left",function(){
    let carusel = $(this).parents('.carousel-viewed');
    left_carusel_viewed(carusel);
    return false;
});
function left_carusel_viewed(carusel){
    let block_width = $(carusel).find('.carousel-viewed__block').outerWidth();
    $(carusel).find(".carousel-viewed__items .carousel-viewed__block").eq(-1).clone().prependTo($(carusel).find(".carousel-viewed__items"));
    $(carusel).find(".carousel-viewed__items").css({"left":"-"+block_width+"px"});
    $(carusel).find(".carousel-viewed__items .carousel-viewed__block").eq(-1).remove();
    $(carusel).find(".carousel-viewed__items").animate({left: "0px"}, 200);

}
function right_carusel_viewed(carusel){
    let block_width = $(carusel).find('.carousel-viewed__block').outerWidth();
    $(carusel).find(".carousel-viewed__items").animate({left: "-"+ block_width +"px"}, 200, function(){
        $(carusel).find(".carousel-viewed__items .carousel-viewed__block").eq(0).clone().appendTo($(carusel).find(".carousel-viewed__items"));
        $(carusel).find(".carousel-viewed__items .carousel-viewed__block").eq(0).remove();
        $(carusel).find(".carousel-viewed__items").css({"left":"0px"});
    });
}